import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontEndDevelopmentComponent } from './front-end-development.component';

describe('FrontEndDevelopmentComponent', () => {
  let component: FrontEndDevelopmentComponent;
  let fixture: ComponentFixture<FrontEndDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontEndDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontEndDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
