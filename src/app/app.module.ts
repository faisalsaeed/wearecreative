import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxUiLoaderModule, NgxUiLoaderRouterModule, NgxUiLoaderConfig, SPINNER, POSITION, PB_DIRECTION } from  'ngx-ui-loader';
import { OwlModule } from 'ngx-owl-carousel';

/* import { HashLocationStrategy, LocationStrategy } from '@angular/common'; */


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { GetInTouchComponent } from './get-in-touch/get-in-touch.component';
import { WhoWeAreComponent } from './who-we-are/who-we-are.component';
import { WhatWeOfferComponent } from './what-we-offer/what-we-offer.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';
import { HomeComponent } from './home/home.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { CaseStudiesComponent } from './case-studies/case-studies.component';
import { FrontEndDevelopmentComponent } from './services/front-end-development/front-end-development.component';
import { BackEndDevelopmentComponent } from './services/back-end-development/back-end-development.component';
import { PageComponent } from './page/page.component';
import { GetStartedComponent } from './get-started/get-started.component';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  "blur": 5,
  "fgsColor": "#d90429",
  "fgsPosition": "center-center",
  "fgsSize":60,
  "fgsType": "double-bounce",
  "overlayColor": "rgba(40, 40, 40, 0.9)",
  "hasProgressBar": false,
  "text": "",
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "threshold": 500
};


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    GetInTouchComponent,
    WhoWeAreComponent,
    WhatWeOfferComponent,
    WhatWeDoComponent,
    HomeComponent,
    PagenotfoundComponent,
    CaseStudiesComponent,
    FrontEndDevelopmentComponent,
    BackEndDevelopmentComponent,
    PageComponent,
    GetStartedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    OwlModule,
    FormsModule,
    HttpClientModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderRouterModule
  ],
  providers: [/* {provide: LocationStrategy, useClass: HashLocationStrategy} */],
  bootstrap: [AppComponent]
})
export class AppModule { }
