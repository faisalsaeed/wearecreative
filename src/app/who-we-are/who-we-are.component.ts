import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import Typed from 'typed.js';
@Component({
  selector: 'app-who-we-are',
  templateUrl: './who-we-are.component.html',
  styleUrls: ['./who-we-are.component.scss']
})
export class WhoWeAreComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    const options = {
      strings: ['understand you','work passionately','work together','are executer','built digital','make  sense','are creative'],
      typeSpeed: 100,
      backSpeed: 100,
      showCursor: true,
      cursorChar: '_',
      loop: true
    };
    const typed = new Typed('.text-rotate-wrap__item', options);

    var sourceSwap = function () {
      var $this = $(this);
      var newSource = $this.data('alt-src');
      $this.data('alt-src', $this.attr('src'));
      $this.attr('src', newSource);
    };
    $('.our-teams__item').hover(function () {
      $('img', this).attr('src', sourceSwap)
    }, function () {
      $('img', this).attr('src', sourceSwap)
    });

    var a = 0;
    $(window).scroll(function () {
      var oTop = $('#counter').offset().top - window.innerHeight;
      if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function () {
          var $this = $(this),
            countTo = $this.attr('data-count');
          $({
            countNum: $this.text()
          }).animate({
            countNum: countTo
          },
            {
              duration: 5000,
              easing: 'swing',
              step: function () {
                $this.text(Math.floor(this.countNum));
              },
              complete: function () {
                $this.text(this.countNum);
                //alert('finished');
              }

            });
        });
        a = 1;
      }

    });




  }
}
