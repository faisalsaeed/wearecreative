import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GetInTouchComponent } from './get-in-touch/get-in-touch.component';
import { WhoWeAreComponent } from './who-we-are/who-we-are.component';
import { WhatWeOfferComponent } from './what-we-offer/what-we-offer.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';
import { CaseStudiesComponent } from './case-studies/case-studies.component';
import { GetStartedComponent } from './get-started/get-started.component';
import { FrontEndDevelopmentComponent } from './services/front-end-development/front-end-development.component';
import { BackEndDevelopmentComponent } from './services/back-end-development/back-end-development.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

//This is my case 
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'get-in-touch', component: GetInTouchComponent },
  { path: 'who-we-are', component: WhoWeAreComponent },
  { path: 'what-we-offer', component: WhatWeOfferComponent },
  { path: 'what-we-do', component: WhatWeDoComponent },
  { path: 'case-studies', component: CaseStudiesComponent },
  { path: 'get-started', component: GetStartedComponent },
  { path: 'services/front-end-development', component: FrontEndDevelopmentComponent },
  { path: 'services/back-end-development', component: BackEndDevelopmentComponent },
  { path: '**', component: PagenotfoundComponent },

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }

