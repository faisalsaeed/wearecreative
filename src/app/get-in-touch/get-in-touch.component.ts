import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from '../contact';
import { ContactService } from '../contact.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-get-in-touch',
  templateUrl: './get-in-touch.component.html',
  styleUrls: ['./get-in-touch.component.scss']
})
export class GetInTouchComponent implements OnInit {
  model = new Contact();
  submitted = false;
  error: {};

  constructor(
    private router: Router,
    private cmspageService: ContactService
  ) { }

  ngOnInit() {

    $(window).mousemove(function(e) {     
      $(".cursor").css({
        left: e.pageX,
        top: e.pageY
      })    
    })
    $(".cursor-link")
      .on("mouseenter", function() {   
      $('.cursor').addClass("active")   
    })
    .on("mouseleave", function() {    
      $('.cursor').removeClass("active")    
    })
  }
  

  onSubmit() {
    this.submitted = true;
    return this.cmspageService.contactForm(this.model).subscribe(
      data => this.model = data,
      error => this.error = error
    );
  }

}
