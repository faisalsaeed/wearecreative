import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import * as $ from 'jquery';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }


  ngOnInit() {

    $(document).ready(function () {

      

      setTime();
      setInterval(function () {
        setTime();
      }, 1000);
      function setTime() {
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();

        var hour = 360 * (h / 12);
        var minute = 360 * (m / 60);
        var second = 360 * (s / 60);

        document.getElementById("hour").style.transform = 'rotate(' + hour + 'deg)';
        document.getElementById("minute").style.transform = 'rotate(' + minute + 'deg)';
        document.getElementById("second").style.transform = 'rotate(' + second + 'deg)';
      }


    

      $(window).on("scroll", function () {
        if ($(window).scrollTop() >= 500) {
          $("header").addClass("header-compressed");
        } else {
          $("header").removeClass("header-compressed");
        }
      });


      $('#toggle').click(function () {
        $(this).toggleClass('active');
        $('#overlay').toggleClass('open');
      });

    });

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        $("#overlay").removeClass("open");
        $("#toggle").removeClass("active");
      }
    });

  }
}
