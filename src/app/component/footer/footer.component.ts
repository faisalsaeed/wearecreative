import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(function () {

      $('.footer__top__button').click(function () {
        $("html, body").animate({
          scrollTop: 0
        }, 600);
        return false;
      });

    });

  }


}
