import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import Typed from 'typed.js';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    const options = {
      strings: ['understand you','work passionately','work together','are executer','built digital','make  sense','are creative'], 
      typeSpeed: 130,
      backSpeed: 100,
      showCursor: true,
      cursorChar: '_',
      loop: true
    };
    const typed = new Typed('.txt-rotate', options);

    var sourceSwap = function () {
      var $this = $(this);
      var newSource = $this.data('alt-src');
      $this.data('alt-src', $this.attr('src'));
      $this.attr('src', newSource);
    };
    $('.item').hover(function () {
      $('img', this).attr('src', sourceSwap)
    }, function () {
      $('img', this).attr('src', sourceSwap)
    });


    $(window).mousemove(function(e) {     
      $(".cursor").css({
        left: e.pageX,
        top: e.pageY
      })    
    })
    $(".cursor-link")
      .on("mouseenter", function() {   
      $('.cursor').addClass("active")   
    })
    .on("mouseleave", function() {    
      $('.cursor').removeClass("active")    
    })



  }

}
