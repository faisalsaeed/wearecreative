import { Component, OnInit } from '@angular/core';
import { PortfolioService } from '../portfolio.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-case-studies',
  templateUrl: './case-studies.component.html',
  styleUrls: ['./case-studies.component.scss']
})
export class CaseStudiesComponent implements OnInit {

  public PortfolioData: any = [];

	constructor(private portfolioService: PortfolioService) {
		this.portfolioService.getJSON().subscribe(data => {
			this.PortfolioData = data;
		});
	}

  ngOnInit() {

    $(window).mousemove(function(e) {     
      $(".cursor").css({
        left: e.pageX,
        top: e.pageY
      })    
    })
    $(".cursor-link")
      .on("mouseenter", function() {   
      $('.cursor').addClass("active")   
    })
    .on("mouseleave", function() {    
      $('.cursor').removeClass("active")    
    })

  }

}
